Since version 1.5 this is a transitional package to transparently
install **doublex**. It provides pyDoubles API by means a wrapper.

This source provides package for the pyDoubles `PYPI project <https://pypi.python.org/pypi/pyDoubles>`__.

Documentation: `<http://pydoubles.readthedocs.org/>`__

.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
